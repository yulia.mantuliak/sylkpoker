$(document).ready(function() {
	$('#form').on('submit', function(event) {
		event.preventDefault()
		//send filled data
		$.ajax({
			type: "POST",
			url: "php/form-sender.php",
			data: $(this).serialize()
		}).done(function() {
			//clear inputs and reset button
			$(this).find('input').val('');
			$('#form').trigger('reset');
			$('.submit_button').attr("value", "Thank you!")
			setTimeout(function(){ $('.submit_button').attr("value", "Sign up"); }, 3000);
			console.log("done")
		});
		return false;
	});
});
		

        function OnImageLoaded (img) {
			$( ".body" ).css( "background-image", "url(" + img.src + ")" )
		}

        function PreloadImage (src) {
            var img = new Image ();
			img.onload = function () {OnImageLoaded (this)};
            img.src = src;
        }

		PreloadImage ("content/bg.png");
		
		$(window).on("load", function () {
		var scene = document.getElementById('js-parallax-scene');
		var parallax = new Parallax(scene);		
			
		})

